package ss.arena.rest.server.resources;

import java.util.List;

public class TodoList {
    private List<Todo> todos;
    
    public List<Todo> getTodos() {
        return todos;
    }
    
    public void setTodos(List<Todo> todos) {
        this.todos = todos;
    }
}
