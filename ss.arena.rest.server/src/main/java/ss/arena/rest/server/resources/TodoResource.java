package ss.arena.rest.server.resources;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("todo")
public class TodoResource {
    private static List<Todo> todoList = new ArrayList<>();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public TodoList getTodos() {
        TodoList response = new TodoList();
        response.setTodos(todoList);
        return response;
    }
    
    @Path("{summary}")
    @GET
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Todo getTodo(@PathParam("summary") String summary) {
        return todoList.stream()
            .filter(todo -> todo.getSummary().equals(summary))
            .findAny()
            .orElse(null);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addTodo(@FormParam("summary") String summary,
                            @FormParam("description") String description,
                            @FormParam("dueDate") String dueDate) {
        Todo todo = new Todo(summary, description);
        todo.setDueDate(LocalDate.parse(dueDate));
        todoList.add(todo);
        return Response.status(Status.OK).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateTodo(Todo todo) {
        String summary = todo.getSummary();
        Optional<Todo> old = todoList.stream()
            .filter(t -> t.getSummary().equals(summary))
            .findAny();
        if (old.isPresent()) {
            old.get().setDescription(todo.getDescription());
            old.get().setDueDate(todo.getDueDate());
            return Response.status(Status.OK).entity(Entity.entity(old.get(), MediaType.APPLICATION_JSON)).build();
        } else {
            return Response.status(Status.NOT_FOUND).entity("Todo with summary " + summary + " not found").build();
        }
    }
    
    @Path("clear")
    @POST
    public Response clear() {
        todoList.clear();
        return Response.status(Status.OK).build();
    }
}
