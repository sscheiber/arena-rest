package ss.arena.rest.server.resources;

import java.time.LocalDate;

public class Todo {
    private String summary;
    private String description;
    private LocalDate dueDate;

    public Todo() {
        // needed by Moxy
    }
    
    public Todo(String summary, String description) {
        this.summary = summary;
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }
    
    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }
}
