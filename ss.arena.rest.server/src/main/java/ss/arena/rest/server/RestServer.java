package ss.arena.rest.server;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.net.httpserver.HttpServer;

import ss.arena.rest.server.resources.TodoResource;

@SuppressWarnings("restriction")
public class RestServer {
    private static final String DEFAULT_HOST = "http://localhost/";
    private static final int DEFAULT_PORT = 9000;
    private static final int DEFAULT_DELAY = 1;
    
    private final HttpServer server;
    
    public RestServer() {
        this(DEFAULT_HOST, DEFAULT_PORT);
    }
    
    public RestServer(String host, int port) {
        URI baseUri = UriBuilder.fromUri(host).port(port).build();
        ResourceConfig resourceConfig = new ResourceConfig(TodoResource.class);
        server = JdkHttpServerFactory.createHttpServer(baseUri, resourceConfig);
    }
    
    public void start() {
        server.start();
    }
    
    public void stop() {
        server.stop(DEFAULT_DELAY); 
    }
}
