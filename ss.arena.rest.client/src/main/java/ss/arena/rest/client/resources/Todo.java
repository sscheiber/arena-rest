package ss.arena.rest.client.resources;

import java.time.LocalDate;

public class Todo {
    private String summary;
    private String description;
    private LocalDate dueDate;
    
    public String getSummary() {
            return summary;
    }
    
    public void setSummary(String summary) {
            this.summary = summary;
    }
    
    public String getDescription() {
            return description;
    }
    
    public void setDescription(String description) {
            this.description = description;
    }
}
