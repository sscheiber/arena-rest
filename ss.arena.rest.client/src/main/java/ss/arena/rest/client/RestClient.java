package ss.arena.rest.client;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ss.arena.rest.client.resources.Todo;
import ss.arena.rest.client.resources.TodoList;

public class RestClient {
    private static final String DEFAULT_HOST = "http://localhost/";
    private static final int DEFAULT_PORT = 9000;
    
    private final WebTarget target;
    
    public RestClient() {
        this(DEFAULT_HOST, DEFAULT_PORT);
    }
    
    public RestClient(String host, int port) {
        URI baseUri = UriBuilder.fromUri(host).port(port).build();
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        target = client.target(baseUri);
    }
    
    public List<Todo> getTodoList() {
        TodoList todoList = target.path("todo").request().get(TodoList.class);
        return todoList.getTodos();
    }
    
    public Todo getTodo(String summary) {
        return target.path("todo").path(summary).request().get(Todo.class);
    }
    
    public void addTodo(String summary, String description, LocalDate dueDate) {
        Form form = new Form("summary", summary)
                .param("description", description)
                .param("dueDate", dueDate.toString());
        target.path("todo").request().post(Entity.form(form), Todo.class);
    }
    
    public boolean updateTodo(Todo todo) {
        Response response = target.path("todo").request().put(Entity.entity(todo, MediaType.APPLICATION_JSON), Response.class);
        return response.getStatus() == Status.OK.getStatusCode();
    }
    
    public Todo getTodo() {
        return target.path("todo").request().get(Todo.class);
    }
    
    public void clear() {
        target.path("todo").path("clear").request().post(Entity.text("clear"));
    }
}
