package ss.arena.rest.client.resources;

import java.util.List;

public class TodoList {
    private List<Todo> todos;
    
    public List<Todo> getTodos() {
        return todos;
    }
    
    public void setTodos(List<Todo> todos) {
        this.todos = todos;
    }
}
