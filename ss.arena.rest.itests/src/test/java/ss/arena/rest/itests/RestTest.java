package ss.arena.rest.itests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;

import ss.arena.rest.client.RestClient;
import ss.arena.rest.client.resources.Todo;
import ss.arena.rest.server.RestServer;

public class RestTest {
    private static RestServer server = new RestServer();
    private RestClient client = new RestClient();
    
    @AfterClass
    public static void tearDownClass() {
        server.stop();
    }
    
    @After
    public void tearDown() {
        client.clear();
    }
    
    @Test
    public void testGetTodo() {
        client.addTodo("Todo 1", "do this", LocalDate.now());
        client.addTodo("Todo 2", "do that", LocalDate.now());
        Todo todo = client.getTodo("Todo 1");
        assertEquals("do this", todo.getDescription());
    }
    
    @Test
    public void testGetTodoNotFound() {
        client.addTodo("Todo 1", "do this", LocalDate.now());
        Todo todo = client.getTodo("Todo 2");
        assertNull(todo);
    }
    
    @Test
    public void testUpdateTodo() {
        client.addTodo("Todo 1", "do this", LocalDate.now());
        Todo todo = client.getTodo("Todo 1");
        todo.setDescription("do that");
        client.updateTodo(todo);
        todo = client.getTodo("Todo 1");
        assertEquals("do that", todo.getDescription());
    }
    
    @Test
    public void testUpdateTodoNotFound() {
        client.addTodo("Todo 1", "do this", LocalDate.now());
        Todo todo = client.getTodo("Todo 1");
        todo.setDescription("do that");
        assertFalse(client.updateTodo(todo));
    }
    
    @Test
    public void testGetTodoList() {
        client.addTodo("Todo 1", "do this", LocalDate.now());
        client.addTodo("Todo 2", "do that", LocalDate.now());
        assertEquals(2, client.getTodoList().size());
    }
}